# OpenML dataset: WaveformDatabaseGenerator

https://www.openml.org/d/4551

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Abstract: CART book's waveform domains
Source:

Original Owners: 

Breiman,L., Friedman,J.H., Olshen,R.A., &amp; Stone,C.J. (1984). 
Classification and Regression Trees. Wadsworth International 
Group: Belmont, California. (see pages 43-49). 

Donor: 

David Aha


Data Set Information:

Notes: 
-- 3 classes of waves 
-- 21 attributes, all of which include noise 
-- See the book for details (49-55, 169) 
-- waveform.data.Z contains 5000 instances


Attribute Information:

-- Each class is generated from a combination of 2 of 3 &quot;base&quot; waves 
-- Each instance is generated f added noise (mean 0, variance 1) in each attribute 
-- See the book for details (49-55, 169)


Relevant Papers:

Leo Breiman, Jerome H. Friedman, Adam Olshen, Jonathan Stone. &quot;Classification and Regression Trees.&quot; 1984. 
[Web Link]

Citation Request:

Please refer to the Machine Learning Repository's citation policy

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4551) of an [OpenML dataset](https://www.openml.org/d/4551). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4551/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4551/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4551/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

